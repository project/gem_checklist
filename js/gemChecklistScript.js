/**
 * @file
 * Js for Gem Checklist.
 */
(function ($, Drupal, drupalSettings) {

  'use strict';

  Drupal.behaviors.gemChecklist = {
    attach: function (context) {
      // Click on a close button to hide the current list item.
      var close = document.getElementsByClassName("close");
      var i;
      for (i = 0; i < close.length; i++) {
        close[i].onclick = function () {
          var data = $.prepareData(this, 'close');
          data.delta = parseInt($(this).attr('data-delta'));
          var output = $.processAjax(data);
          if (output.status == 200) {
            $(this).parent().hide();
            return;
          }
        }
      }

      // Add a "checked" symbol when clicking on a list item.
      $("#gemchecklist li").once().on('click', function (ev) {
        if (ev.target !== this) {
          return;
        }
        // Get the opposite class as the actual toggling will perform later.
        var toggleState = $(ev.target).hasClass('checked') ? 'unchecked' : 'checked';
        var data = $.prepareData(ev.target, toggleState);
        data.delta = parseInt($(ev.target).find('span').attr('data-delta'));
        if ($.isNumeric(data.delta) != true) {
          return;
        }
        var output = $.processAjax(data);
        if (output.status == 200) {
          ev.target.classList.toggle('checked');
        }
      });

      // Prepare request data for given element.
      $.prepareData = function (ele, action) {
        var div = $(ele).closest('div');
        var fieldName = $(div).attr('data-field');
        return {
          id: drupalSettings.checklists.currentNid,
          field: fieldName,
          op: action
        }
      };

      // Process ajax request.
      $.processAjax = function (data) {
        return $.ajax({
          async: false,
          url: Drupal.url('gem-checklist/' + data.id),
          type: "POST",
          data: JSON.stringify(data),
          contentType: "application/json; charset=utf-8",
          dataType: 'json'
        });
      };

      // Filter topic in the list.
      $.filterTopics = function () {
        $(context).find("#gemChecklistHeader input.search-topic").on("keyup", function () {
          var value = $(this).val().toLowerCase();
          $("li.gemchecklist-topic").filter(function () {
            $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
          });
        });
      }
      $.filterTopics();
    }
  };
})(jQuery, Drupal, drupalSettings);
