CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Troubleshooting
 * Maintainers


INTRODUCTION
------------

The Gem CheckList allows user to create a checklist in a Gem CheckList type field of a normal content entity.

  The item added in the checklist is called as Topic and it can be added like any other field data. Once a topic get completed, user can mark it as
  completed in the field itself. It is very easy to install as well as to use
  fully flexible to make it interactive with no dependencies except drupal core.


 * For a full description of the module visit:
   https://www.drupal.org/project/gem_checklist

 * To submit bug reports and feature suggestions, or to track changes visit:
   https://www.drupal.org/project/issues/gem_checklist


REQUIREMENTS
------------

This module requires no modules outside of Drupal core.


INSTALLATION
------------

 * Install the Gem CheckList module as you would normally install a contributed
   Drupal module.
   Visit https://www.drupal.org/node/1897420 for further information.


CONFIGURATION
-------------

  Goto add new field on any content type.
  
  You will see a new type of field called Gem Checklist.
  
  Create a Gem Checklist field.
  
  Set field cardinality as needed and hit save settings.
  
  Add a new content and add Topics into your Gem Checklist fields.
  
  If you want to override the default styles you can just open the
  ADDITIONAL STYLES tab which is just below the topic field.

  Allow user to delete the topics from the front end by allowing him
  'Delete Checklist Topics' permission.

  Save the content and view it on the front end.



TROUBLESHOOTING
---------------

Check if your module installation was successful and clear the site cache once.
If still an issue, feel free to report it on module's issue page, as mentioned.

MAINTAINERS
-----------

  * Shobhit Juyal (https://www.drupal.org/u/shobhit_juyal)

