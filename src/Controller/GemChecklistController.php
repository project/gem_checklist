<?php

namespace Drupal\gem_checklist\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\node\NodeInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;

/**
 * Defines GemChecklistController class.
 */
class GemChecklistController extends ControllerBase {

  /**
   * Return the output.
   *
   * @param \Drupal\node\NodeInterface $node
   *   Block class object param.
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   Current request object.
   *
   * @return \Symfony\Component\HttpFoundation\Response
   *   Return response object.
   */
  public function handle(NodeInterface $node, Request $request) {
    $params = [];
    $content = $request->getContent();
    if (!empty($content)) {
      // 2nd param to get as array
      $params = json_decode($content, TRUE);
    }
    $response = new Response();
    // Validate the node and user permissions.
    if ($params && $node
      && $this->currentUser()->hasPermission('access content')
      && ($params['id'] == $node->id())) {
      $params['field'] = str_replace('-', '_', $params['field']);
      if ($node->hasField($params['field']) &&
        $this->processContent($node, $params)) {
        return $response;
      }
    }

    return $response->create(
      'FALSE',
      Response::HTTP_BAD_REQUEST)
      ->setSharedMaxAge(300);
  }

  /**
   * Process block operation.
   */
  protected function processContent(&$node, $params) {
    $items = $node->get($params['field'])->getValue();
    if (!$items || !isset($items[$params['delta']])) {
      return FALSE;
    }
    if ($params['op'] == 'close' &&
      $this->currentUser()
        ->hasPermission('delete checklist topics')) {
      // Remove particular index and re-set the values.
      unset($items[$params['delta']]);
    }
    elseif ($params['op'] == 'checked') {
      $items[$params['delta']]['completed'] = 1;
    }
    elseif ($params['op'] == 'unchecked') {
      $items[$params['delta']]['completed'] = 0;
    }
    if ($items === $node->get($params['field'])->getValue()) {
      return FALSE;
    }
    $node->set($params['field'], $items);

    return $node->save();
  }

}
