<?php

namespace Drupal\gem_checklist\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FormatterBase;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Component\Utility\Html;

/**
 * Plugin implementation of the 'gem_checklist_formatter' formatter.
 *
 * @FieldFormatter(
 *   id = "gem_checklist_formatter",
 *   label = @Translation("Gem CheckList"),
 *   field_types = {
 *     "gem_checklist"
 *   }
 * )
 */
class GemCheckListFormatter extends FormatterBase {

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    // Early opt-out if the field is empty.
    if (count($items) <= 0) {
      return [];
    }

    $elements = [];

    foreach ($items as $delta => $item) {
      // Get additional styles for the topics.
      $additional_styles = unserialize($item->get('styles')->getValue()) ?: '';
      $styles = [];
      if ($additional_styles) {
        foreach ($additional_styles as $key => $style) {
          if ($key == 'font_size') {
            $style .= 'px';
          }
          $styles[] = str_replace('_', '-', $key) . ': ' . $style . ';';
        }
        $styles = implode(' ', $styles);
      }

      $elements[$delta] = [
        '#theme' => 'gemchecklist',
        '#topic' => Html::escape($item->topic),
        '#completed' => (int) $item->completed,
        '#styles' => $styles,
        '#delta' => (int) $delta,
      ];
    }
    return [
      [
        '#theme' => 'gemchecklists',
        '#elements' => $elements,
        '#field' => Html::getId($items->getName()),
        '#attached' => [
          'library' => [
            'gem_checklist/gem_checklist.list',
          ],
        ],
      ],
    ];
  }

}
