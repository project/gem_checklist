<?php

namespace Drupal\gem_checklist\Plugin\Field\FieldWidget;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\WidgetBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Field\WidgetInterface;
use Drupal\Component\Utility\Color;

/**
 * Plugin implementation of the 'gem_checklist_widget' widget.
 *
 * @FieldWidget(
 *   id = "gem_checklist_widget",
 *   module = "gem_checklist",
 *   label = @Translation("Gem CheckList Widget"),
 *   field_types = {
 *     "gem_checklist"
 *   }
 * )
 */
class GemCheckListWidget extends WidgetBase implements WidgetInterface {

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {

    $cardinality = $this->fieldDefinition->getFieldStorageDefinition()->getCardinality();

    $element['data'] = [
      '#type' => 'table',
      '#header' => [
        'topic' => t('Topic'),
        'status' => t('Status'),
      ],
      '#empty' => 'No result to display',
    ];
    // $element['topic'] = [
    $element['data'][$delta]['topic'] = [
      '#type' => 'textfield',
      '#title' => $cardinality == 1 ? $this->fieldDefinition->getLabel() :
      $this->t('Topic'),
      '#required' => $element['#required'],
      '#title_display' => 'invisible',
      '#size' => 50,
      '#default_value' => isset($items[$delta]->topic) ? $items[$delta]->topic : '',
      '#description' => $this->t('Name of the checklist topic.'),
      '#element_validate' => [
        [static::class, 'validateTopicName'],
      ],
    ];

    // $element['completed'] = [
    $element['data'][$delta]['completed'] = [
      '#type' => 'checkbox',
      '#title' => $cardinality == 1 ? $this->fieldDefinition->getLabel() : $this->t('Completed'),
      '#default_value' => isset($items[$delta]->completed) ? $items[$delta]->completed : 0,
    ];

    // Get current settings.
    $stylesSettings = unserialize($items[$delta]->get('styles')->getValue());

    // Build additional styles.
    $element['styles'] = [
      '#type' => 'details',
      '#open' => FALSE,
      '#title' => $this->t('Additional Styles'),
      '#description' => $this->t("The styles will be applied on the topic."),
    ];
    $element['styles']['background'] = [
      '#type' => 'textfield',
      '#title' => $this->t("Background Color"),
      '#description' => $this->t("Should be a valid hex code. For eg. #000"),
      '#size' => 50,
      '#default_value' => isset($stylesSettings['background']) ? $stylesSettings['background'] : FALSE,
      '#element_validate' => [
          [static::class, 'validateHex'],
      ],
    ];
    $element['styles']['color'] = [
      '#type' => 'textfield',
      '#title' => $this->t("Fonts Color"),
      '#description' => $this->t("Should be a valid hex code. For eg. #000"),
      '#size' => 50,
      '#default_value' => isset($stylesSettings['color']) ? $stylesSettings['color'] : FALSE,
      '#element_validate' => [
          [static::class, 'validateHex'],
      ],
    ];
    $element['styles']['font_family'] = [
      '#type' => 'textfield',
      '#title' => $this->t("Fonts family"),
      '#size' => 50,
      '#default_value' => isset($stylesSettings['font_family']) ? $stylesSettings['font_family'] : FALSE,
      '#description' => $this->t("Please ensure if the font family exists."),
    ];
    $options = ['' => $this->t("Select")];
    $element['styles']['font_size'] = [
      '#type' => 'select',
      '#title' => $this->t("Fonts size"),
      '#options' => array_merge($options,
      array_combine(range(12, 20, .5), range(12, 20, .5))),
      '#default_value' => isset($stylesSettings['font_size']) ? $stylesSettings['font_size'] : FALSE,
      '#description' => $this->t("Font sizes are in pixels."),
    ];
    $element['styles']['font_weight'] = [
      '#type' => 'select',
      '#title' => $this->t("Fonts weight"),
      '#options' => array_merge($options,
      array_combine(range(100, 1000, 100), range(100, 1000, 100))),
      '#default_value' => isset($stylesSettings['font_weight']) ? $stylesSettings['font_weight'] : FALSE,
    ];
    $element['styles']['text_align'] = [
      '#type' => 'select',
      '#title' => $this->t("Text alignment"),
      '#options' => [
        '' => $this->t("Select"),
        'left' => $this->t("Left"),
        'right' => $this->t("Right"),
        'center' => $this->t("Center"),
        'justify' => $this->t("Justify"),
      ],
      '#default_value' => isset($stylesSettings['text_align']) ? $stylesSettings['text_align'] : FALSE,
    ];

    return $element;
  }

  /**
   * Validate the topic name.
   */
  public static function validateTopicName($element, FormStateInterface $form_state) {
    $value = $element['#value'];
    if (strlen($value) == 0) {
      $form_state->setValueForElement($element, '');
    }
  }

  /**
   * Validates hex color code.
   */
  public static function validateHex($element, FormStateInterface $form_state) {
    if ($element['#value'] &&
      FALSE === Color::validateHex($element['#value'])) {
      $form_state->setError($element, t("Invalid hex code."));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function massageFormValues(array $values, array $form, FormStateInterface $form_state) {
    // Loop over each item and set the data.
    foreach ($values as $key => &$item) {
      $item['topic'] = $item['data'][$key]['topic'];
      $item['completed'] = $item['data'][$key]['completed'];
      // Remove data key.
      unset($item['data']);
      $item['styles'] = $item['styles'] ?
                      array_filter($item['styles']) : [];
      $item['styles'] = serialize($item['styles']);
    }

    return $values;
  }

}
