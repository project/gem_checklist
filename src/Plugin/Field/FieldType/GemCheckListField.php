<?php

namespace Drupal\gem_checklist\Plugin\Field\FieldType;

use Drupal\Core\Field\FieldItemBase;
use Drupal\Core\Field\FieldItemInterface;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\TypedData\DataDefinition;

/**
 * Provides a field type of Gem CheckList.
 *
 * @FieldType(
 *   id = "gem_checklist",
 *   label = @Translation("Gem CheckList"),
 *   module = "gem_checklist",
 *   category = @Translation("Text"),
 *   description = @Translation("Gem CheckList"),
 *   default_formatter = "gem_checklist_formatter",
 *   default_widget = "gem_checklist_widget"
 * )
 */
class GemCheckListField extends FieldItemBase implements FieldItemInterface {

  /**
   * {@inheritdoc}
   */
  public static function schema(FieldStorageDefinitionInterface $field_definition) {
    return [
      // Columns contains the values that the field will store.
      'columns' => [
        'topic' => [
          'type' => 'text',
          'size' => 'normal',
          'not null' => TRUE,
        ],
        'completed' => [
          'type' => 'int',
          'unsigned' => TRUE,
          'not null' => TRUE,
          'size' => 'tiny',
          'default' => 0,
          'description' => t('0 for incomplete, 1 for completed.'),
        ],
        'styles' => [
          'type' => 'text',
          'size' => 'normal',
          'not null' => FALSE,
        ],
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function isEmpty() {
    $topic = $this->get('topic')->getValue();
    return ($topic === NULL || $topic === '');
  }

  /**
   * {@inheritdoc}
   */
  public static function propertyDefinitions(FieldStorageDefinitionInterface $field_definition) {
    $properties = [];
    $properties['topic'] = DataDefinition::create('string')
      ->setLabel(t('Topic'))
      ->setDescription(t('The topic of the list.'));
    $properties['completed'] = DataDefinition::create('integer')
      ->setLabel(t('Completed'))
      ->setDescription(t('The completed status.'));
    $properties['styles'] = DataDefinition::create('string')
      ->setLabel(t('Styles'))
      ->setDescription(t('The additional styles to apply.'));

    return $properties;
  }

}
